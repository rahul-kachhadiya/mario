﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour, ICameraMovement
{

    public Rigidbody2D Player;
    public void Move(Vector2 point)
    {
        if(Player.position.x <= 0) return;
        transform.position = new Vector3(point.x,transform.position.y, transform.position.z);
    }

    
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        Move(Player.position);
    }
}
