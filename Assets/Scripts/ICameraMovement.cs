using UnityEngine;
public interface ICameraMovement{
    void Move(Vector2 point);
}