using UnityEngine;
using System.Collections;

public interface IPlayerMovement
{
    void Jump();
    void Move(bool isCrouch);
    void Crouch();
    void Walk();

}


