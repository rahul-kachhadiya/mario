﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour, IPlayerMovement
{
    

    [Range(0.01f, 5)]
    public float MaxHorizontalVelocity;
    [Range(0.01f, 3)]
    public float CrouchHorizontalVelocity = 3;
    public Tilemap EnvironmentTileMap = null;
    [Range(1, 15)]
    public int VerticalVelocity = 4;

    private Rigidbody2D rigidBody2D = null;
    private BoxCollider2D playerHeadCollider = null;
    private Animator playerAnimator = null;
    private bool isCrouching = false;
    LayerMask platformMask = 0;

    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        playerHeadCollider = GetComponent<BoxCollider2D>();
        playerAnimator = GetComponent<Animator>();
        platformMask = LayerMask.GetMask("Ground");
    }
    
    void FixedUpdate(){
        
        var verticleInput = Input.GetAxis("Vertical");
        var horizontalInput = Input.GetAxis("Horizontal");

        if(verticleInput > 0.01 && ShouldJump()){
            Jump();
        }


        if (verticleInput < -0.01 || (isCrouching && Physics2D.CircleCast(playerHeadCollider.transform.position,0.3f, Vector3.up,1,platformMask)))
        {
            Crouch();
            isCrouching = true;
        }
        else
        {
            Walk();
            isCrouching = false;
        }

        Move(isCrouch: isCrouching);

        if (horizontalInput > 0){
            transform.eulerAngles = new Vector3(0,0,0);
        }else if(horizontalInput < 0){
            transform.eulerAngles = new Vector3(0,180,0);
        }
        
    }

    void OnCollisionEnter2D(Collision2D other){
        playerAnimator.SetBool("jump",false);
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag.Equals("Finish")){
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    void OnBecameInvisible(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    

    bool IsGrouded(){
        return Physics2D.CircleCast(transform.position,0.2f,Vector3.down , 0.9f,platformMask);
    }

    bool ShouldJump(){
        if(IsGrouded() && rigidBody2D.velocity.y < 9 && !isCrouching){
            return true;
        }

        return false;
    }

    public void Jump()
    {
        float verticleForce =  VerticalVelocity;

        if(verticleForce>=0.01f){
            playerAnimator.SetBool("jump",true);
        }

        rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, verticleForce);
    }

    

    public void Crouch()
    {
        playerHeadCollider.enabled = false;
        playerAnimator.SetBool("crouch", true);
    }

    public void Walk()
    {
        playerHeadCollider.enabled = true;
        playerAnimator.SetBool("crouch", false);
    }

    public void Move(bool isCrouch)
    {
        float horizontalVelocity = Input.GetAxis("Horizontal") * (!isCrouch ?MaxHorizontalVelocity : CrouchHorizontalVelocity);
        
        if (!isCrouch && Physics2D.CircleCast(transform.position - new Vector3(0,0.2f,0) + (transform.right* 0.2f), 0.2f, transform.right, 0f, platformMask))
        {
            horizontalVelocity = 0;
        }

        rigidBody2D.velocity = new Vector2(horizontalVelocity, rigidBody2D.velocity.y);
        playerAnimator.SetFloat("AnimRunning", Mathf.Abs(rigidBody2D.velocity.x));
    }
}
